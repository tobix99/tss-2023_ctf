import hashlib
import re
import sys

keepassString = "echo {pw}"
pattern = re.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\W)")

pattern_sha1 = re.compile("^5ce")
pattern_md4 = re.compile("^6be")

file1 = open('rockyou.txt', 'r', errors='ignore')
Lines = file1.readlines()

file_out = open('out.txt', 'a')


x2 = 0

allLines = len(Lines)
print("starting to hash: file has {} lines".format(allLines))

for currLineNum, line in enumerate(Lines):
    if (currLineNum % 100) == 0:
        sys.stdout.write("\r{:,d}".format(currLineNum) + "/{:,d}".format(allLines) + "\t\t - {val:.2f}%".format(val=currLineNum/allLines*100))
        sys.stdout.flush()
    line = line.rstrip()
    if len(line) < 10:
        continue

    if not pattern.search(line):
        continue

    i = 0
    while i < 2050:
        temp = line + str(i)
        #m.update()
        #hashIs = hashlib.sha1(temp.encode("utf-16-le")).hexdigest()

        m = hashlib.sha1()
        m.update(temp.encode("utf-16-le"))
        hashIs = m.hexdigest()

        if pattern_sha1.search(hashIs):
            hashObject = hashlib.new('md4', temp.encode('utf-16-le'))
            hash2Is = hashObject.hexdigest()

            if pattern_md4.search(hash2Is):
                #file_out.write(m.hexdigest() + " (" + temp + ") \n")
                file_out.write(temp + "\n")
                x2 += 1
        i += 1


print(x2)
file1.close()
file_out.close()
